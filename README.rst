
========
EPS 253:  Glaciology
========

Term: Fall 2018-2019

Course Instructor(s): Bradley Lipovsky

Location: Geological Museum 204 (FAS)

Meeting Time: Friday 09:45 AM - 11:45 AM

Course Description: This course explores the physics of glaciers and ice sheets. We begin by describing �synoptic scale� glaciology, i.e., at the scale of entire glaciers and ice sheets. The course then examines the smaller-scale physics of the glacier-atmosphere, -bed, and -ocean interfaces, the interactions between ice sheets and planetary climates, and the analysis of ice cores.  Special attention will be given to a dozen or so deep-focus topics that may include ice shelf hydraulic fracturing, glacier sliding, the marine ice sheet instability, climatic forcing of ice sheet steady states, and ice core reconstructions. These topics will be approached using mathematical physics, geophysical data, simple computer simulations, and large-scale ice sheet models.


This class will make use of the `Ice Sheet System Model`_.

.. _`Ice Sheet System Model`: http://issm.jpl.nasa.gov/